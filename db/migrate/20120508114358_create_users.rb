class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.string :username
      t.string :hashpass
      t.string :email
      t.string :salt
      t.string :lastfm_username

      t.timestamps
    end
  end
end
