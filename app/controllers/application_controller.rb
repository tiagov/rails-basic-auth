class ApplicationController < ActionController::Base

  protect_from_forgery

  protected

  # Session Methods
  def current_user
    if session[:user_id].nil?
      nil
    else
      begin
        User.find(session[:user_id])
      rescue
        nil
      end
    end
  end

  def set_current_user(user_id)
    session[:user_id] = user_id
  end

  def logged_in?
    !current_user.nil?
  end

  # Gatekeeping
  def controlAccess
    if !logged_in?
      redirect_to :controller => 'session', :action => 'login'
    end
  end

end
