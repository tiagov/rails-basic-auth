require "uri"
require "net/http"
require "Digest" # comment this line when on Linux
# require "digest" # uncomment this line on Linux

class SessionController < ApplicationController

  # Last.fm keys
  LASTFM_KEY =  "b11117a70946f0a03656389e6b67cb94"
  LASTFM_SECRET = "86f6aea505bf7a7c16e1f635b9ec765f"
  LASTFM_CALLBACK = "http://localhost:3000/session/lastfm_cb"
  LASTFM_TOKEN_URL = "http://www.last.fm/api/auth/?api_key="+LASTFM_KEY+"&cb="+LASTFM_CALLBACK

  # FIELD NAMES/PARAMETERS NAME
  USERNAME_FIELD = 'username'
  PASSWORD_FIELD = 'pass'

  # EMPTY JSON RESPONSE
  EMPTY_RES = "".to_json

  # POST /create
  def create

    username = params[USERNAME_FIELD]
    userpass = params[PASSWORD_FIELD]

    if username != "" and userpass != ""
      user = User.find_by_username(username)
    end

    respond_to do |format|

      if logged_in?
        format.json { render json: EMPTY_RES, status: :conflict, location: user}
      elsif !user.nil? and user.authenticate(userpass)
        set_current_user user.id
        format.json { render json: EMPTY_RES, status: :accepted, location: user}
      else
        format.json { render json: EMPTY_RES, status: :forbidden, location: user}
      end

    end

  end

  # POST /delete
  def delete

    if logged_in?
      session[:user_id] = nil
    end

    respond_to do |format|
      format.html { redirect_to :controller => 'session', :action => 'login' }
    end

  end

  def lastfm_cb

    token = params[:token]
    signa = Digest::MD5.hexdigest("api_key"+LASTFM_KEY+"methodauth.getSessiontoken"+token+LASTFM_SECRET)
    uri = URI.parse("http://ws.audioscrobbler.com/2.0/?method=auth.getSession&format=json&token="+token+"&api_key="+LASTFM_KEY+"&api_sig="+signa)
    response = ActiveSupport::JSON.decode(Net::HTTP.get_response(uri).body)["session"]
    lst_user = response["name"]

    session[:lastfm_sk] = response["key"]

    user = User.find_by_lastfm_username lst_user

    # If the user isn't already registered, create a new account for him.

    if user.nil?
      user = User.new
      user.lastfm_username= lst_user
      user.password="shouldberandom"
      user.username= lst_user
      user.save
    end

    set_current_user User.find_by_username(lst_user).id

    respond_to do |format|
      format.html { redirect_to :controller => 'session', :action => 'login' }
    end

  end

  # GET /login
  def login
    @user = current_user
  end

end
