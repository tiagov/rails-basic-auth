// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

$(function()
    {
        ajaxHandlers()
    }
);


function ajaxHandlers()
{
    $('#login')
            .bind("ajax:beforeSend", function(evt, xhr, settings)
            {
                var formContent = $("#login-form").serialize();

                if(formContent.length <= 100)
                {
                    alert("Provide an username and a password!");
                    return false;
                }
            })
            .bind("ajax:success", function(evt, data, status, xhr)
            {
                alert("Successful login!");
                setTimeout(function(){window.location.reload()}, 500);
            })
            .bind("ajax:error", function(evt, xhr, status, error)
            {
                alert("Login failed!");
            }
    );
}