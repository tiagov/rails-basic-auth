require "digest/sha2"

class User < ActiveRecord::Base

  # Attributes
  attr_accessible :email, :username
  attr_protected :hashpass, :salt

  # Callbacks
  before_save :hash_the_pass

  def password=(pass)
    @password = pass
  end

  def password
    @password
  end

  def authenticate(pass)
    self.hashpass == encrypt(pass, self.salt)
  end

  private

  # Hashes the password (meant to be called before storing the object)
  def hash_the_pass

  self.salt = random_string 12
  self.hashpass = encrypt @password, salt

  end

  # Encrypts a given string using a given salt
  def encrypt(pass, salt)
    Digest::SHA2.hexdigest(salt+pass)
  end

  # Generates a random string consisting of alphanumeric characters
  def random_string(len)
    chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
    result = ""

    len.times { || result << chars[rand(chars.size-1)] }

    result
  end

end
